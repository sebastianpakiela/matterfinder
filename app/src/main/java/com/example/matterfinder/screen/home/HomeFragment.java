package com.example.matterfinder.screen.home;


import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.matterfinder.R;
import com.example.matterfinder.screen.home.adapter.TripAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeFragment extends Fragment {

    @BindView(R.id.fragment_home_recycler_view)
    protected RecyclerView bestTripsRecyclerView;

    @BindView(R.id.fragment_home_toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.fragment_home_fab)
    protected FloatingActionButton fab;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setSupportActionBar(toolbar);

        ButterKnife.bind(this, view);

        initRecyclerView();
        toolbar.setTitle(R.string.app_name);

        Menu menu = toolbar.getMenu();
        menu.add("SEARCH").setIcon(R.drawable.ic_search_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add("FILTER").setIcon(R.drawable.ic_filter_list_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return view;
    }

    private void initRecyclerView() {
        bestTripsRecyclerView.setAdapter(new TripAdapter());
        bestTripsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    @OnClick(R.id.fragment_home_fab)
    protected void onFabClick() {
        new AlertDialog.Builder(getActivity())
                .setMessage("Dodaj nową relację")
                .setCancelable(true).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
    }
}
